**AffdexMe** is an Android app that demonstrates the use of the Affdex SDK.  It uses the camera on your Android device to view, process and analyze live video of your face. Start the app and you will see your face on the screen and metrics describing your expressions. Tapping the screen will bring up a menu with options to display the Processed Frames Per Second metric, display facial tracking points, and control the rate at which frames are processed by the SDK.

To use this project, you will need to:
- Build the project using Android Studio
- Run the app and smile!
- Please dont anger with me!

If you are interested in learning how the Affectiva SDK works, you will find the calls relevant to the use of the SDK in the initializeCameraDetector(), startCamera(), stopCamera(), and onImageResults() methods.  See the comment section at the top of the MainActivity.java file for more information.

This is a-edited version of affdexMe that speasially detect a anger emotion of someone and automatically play a alarm sound if the anger score is more than 85%

**Fitur Added or remove**
+ add tornado_sound and firepager in raw file
+ change versioncode and versionname to miss the error
+ change to "comentar" off affdexme Logo
+ add AIMP String, and change the loading_string 
+ minimize detect_anger_score parameter to miss the bug
+ build play_sound function
+ start play_sound when anger in >50%
+ make a toast notification when user is anger
+ make it simple

**How to change alarm sound**
1. From file : copy-paste <sound>.mp3 to app>src>main>res>raw (use small letter witout space)
<br>From Android studio : in app>res>raw left click, choose new>file and select the sound

2. In MainActivity.java find "testEmo" fuction (originaly line 863), and change R.raw.<name_sund>
<br>sample : final MediaPlayer mp = MediaPlayer.create(this, R.raw.firepager);


***
Copyright (c) 2016 Affectiva Inc. <br> See the file [license.txt](license.txt) for copying permission.

This app uses some of the excellent [Emoji One emojis](http://emojione.com) and Amazing Sound from [SoundBible](http://soundbible.com)
Edited by : namakuanshar - Solo Fighter

